if(io.sails) {
  io.sails.autoConnect = true;
  io.sails.reconnection = true;
}

var ng = angular.module('myChat', ['ngSails']);

ng.controller('chatController', ['$scope', '$sails', function($scope, $sails){

  document.getElementById('txtMensaje').focus();

  $scope.enCanal = false;
  $scope.eventos = [];
  $scope.clients = [];
  $scope.client = {};
  $scope.conectado = true;

  $scope.enviarMensaje = function(){
    console.info("Enviando mensaje...");
    $sails.post('/mensajes/nuevo',{
      txt: $scope.txtMensaje,
      origen: $scope.client
    }).success(function(response){
      console.log("Mensaje almacenado", response);
      $scope.txtMensaje="";
      document.getElementById('txtMensaje').focus();
    }).error(function(error){
      console.error("Error al guardar el mensaje", error);
    });
  };

  $scope.keyDown = function(event){
    if(event.which==13 && $scope.txtMensaje.length){
      $scope.enviarMensaje();
    }
  };

  $scope.suscribirse = function(){
    $sails.post('/mensajes/join',{nombre: $scope.client.nombre}).success(function(response){
      console.log("Has entrado al canal de programadores.", response);
      $scope.eventos = response.mensajes||[]; //al inicio solo tenemos mensajes
      $scope.clients = response.clients||[]; //los clientes conectados en el canal
      $scope.client = response.client; //mi identidad
      $scope.enCanal = true;
      document.getElementById('txtMensaje').focus();
      $scope.eventos.push({ //agregar mi propio join al canal.
        fecha: response.fecha,
        type: 'join',
        origen: $scope.client
      });
    }).error(function(error){
      console.error("Error al entrar al canal", error);
    });
  };

  $sails.on('msg', function(evento){
    console.log('Mensaje recibido', evento);
    $scope.eventos.push(evento);
  });

  $sails.on('join', function(join){
    console.log('Cliente conectado', join);
    $scope.clients.push(join.client);
    $scope.eventos.push({
      fecha: join.fecha,
      type: 'join',
      origen: join.client
    });
  });

  $sails.on('leave', function(leave){
    console.log('cliente desconectado', leave);
    var idx= _.findIndex($scope.clients, {socket: leave.socket});
    var cliente = {};
    if(idx>-1){
      cliente = angular.copy($scope.clients[idx]);
      $scope.clients.splice(idx, 1);
      $scope.eventos.push({
        fecha: leave.fecha,
        type: 'leave',
        origen: cliente
      });
    }
  });

  $sails.on('disconnect', function(){
    console.log('yo me desconecté!');
    $scope.enCanal = false;
    $scope.clients = [];
    $scope.eventos = [];
    io.sails.connect();
    $scope.conectado = false;
  });

  $sails.on('connect', function(){
    //si ya tengo user, re-conectar automaticamente
    $scope.conectado = true;
    if($scope.client.nombre){
      $scope.suscribirse();
    }
  });

}]);
