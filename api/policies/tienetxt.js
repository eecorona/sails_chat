
module.exports = function(req, res, next) {

  if (req.body.txt && req.body.txt.length) {
    return next();
  }

  return res.forbidden('El mensaje es requerido.');
};
