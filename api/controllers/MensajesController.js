/**
 * MensajesController
 *
 * @description :: Server-side logic for managing mensajes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var moment = require('moment');

module.exports = {
  nuevo: function(req, res){
    Mensajes.create({
      txt: req.body.txt,
      origen: req.body.origen,
    }).exec(function(err, mensaje){
      if(err) return res.serverError(err);
      return res.send(mensaje);
    });
  },
	join: function(req, res){
    if(req.isSocket){
      sails.sockets.join(req, 'chat', function(){
        var socket = sails.sockets.getId(req);
        var client = {nombre: req.body.nombre, socket: socket};
        var fecha = moment().format("YYYY-MM-DD HH:mm:ss");

        var fecha_limite = moment().subtract(10, 'm').format("YYYY-MM-DD HH:mm:ss");
        req.session.user = client;
        //obtener mensajes actuales para enviarselos
        Mensajes.find({ fecha: { '>=': fecha_limite }}).exec(function(err_msg, mensajes) {
          if (err_msg) return res.serverError(err_msg);

          //mandar evento join al canal, excepto a mi
          sails.sockets.broadcast('chat', 'join', { fecha: fecha, client: client }, req);

          //agregar este usuario a los usuarios online
          roomService.addUser(client, function(err_add, conectado){
            if(err_add) return res.serverError(err_add);
            roomService.getClients(function(err_get, conectados){
              if(err_get) return res.serverError(err_get);
              return res.send({
                mensajes: mensajes,
                client: conectado,
                clients: conectados, //regresarle los usuarios en lineas
                fecha: fecha
              });
            });
          });
        });
      });
    }
    else return res.badRequest();
  }
};
