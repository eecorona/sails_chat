var moment = require("moment");
module.exports = {
  addUser: function(user, cb){
    console.log('Agregando usuario', user);
    Conectados.create(user).exec(function(err, conectado){
      if(err) return cb(err, null);
      return cb(null, conectado);
    });
  },
  removeUser: function(socket, cb){
    console.log('quitando socket', socket);
    Conectados.destroy({socket:socket}).exec(function(err, desconectado){
      if(err) return cb(err, null);
      sails.sockets.broadcast('chat', 'leave', {fecha: moment().format("YYYY-MM-DD HH:mm:ss"), socket:socket});
      return cb(null, desconectado);
    });
  },
  getClients: function(cb){
    Conectados.find().exec(function(err, conectados){
      if(err) return cb(err, null);
      return cb(null, conectados);
    });
  }
};
