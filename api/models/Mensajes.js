/**
 * Mensajes.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var moment = require('moment');

module.exports = {

  attributes: {
    txt:{
      type:'string',
      required: true
    },
    type: {
      type: 'string',
      defaultsTo: 'msg'
    },
    fecha: {
      type: 'datetime',
      defaultsTo: moment().format("YYYY-MM-DD HH:mm:ss")
    },
    origen:{
      type: 'json',
      required: true
    }
  },
  afterCreate: function(msg, next){
    sails.sockets.broadcast('chat','msg', msg);
    next();
  }
};
